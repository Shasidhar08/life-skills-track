

# Feynman Technique
## Question 1

### What is the Feynman Technique? 

* A learning method named after the Nobel Prize-winning physicist, Richard Feynman. 
* Designed to help learners gain a deeper understanding of complex concepts by breaking them down into simpler, more manageable parts.
* The Feynman Technique can be applied to a wide range of subjects, from science and math to philosophy and literature. 

The technique involves four steps:
1.  Choose a concept.
2.  Explain it in simple terms.
3.  Identify knowledge gaps.
4.  Fill in the gaps.

## Question 2

### What are the different ways to implement this technique in your learning process?
  

* Step 1: Choose a concept
 * - Select a concept that you want to understand better.
 * -  It could be anything from a science to a philosophy.
* Step 2: Explain it in simple terms
* - Try to explain the concept in simple, easy-to-understand language as if you were teaching it to a child. 
* - Use analogies, metaphors, and examples to make the concept more relatable.
* Step 3: Identify knowledge gaps
* - Identify any areas of the concept that you don't fully understand or that you're unsure about. 
* - This will help you to focus your learning and research efforts.

The Feynman Technique is a powerful tool for improving your understanding of complex concepts, and it can be applied to a wide range of subjects.


# "Learning How to Learn" TED Talk by Barbara Oakley

## Question 3
### Paraphrase the video **in detail** in your own words.
  
Barbara Oakley discusses the science of learning and how our brains process information.
In her video She 
* talks about the importance of taking breaks, and how our brains need time to process and create new neural pathways.
*  also shares tips on how to tackle difficult subjects. 
*  explains how she trained her brain and how anyone can do that. 

#### _Key Points are_
1.  **Focus mode** and **Diffused mode**, which are needed to be switched back and forth for efficiency.

2.  **Dali**, an artist and **Edison**, well known scientist and their approach of imagination.

3.  Do not **Procrastinate**

4.  **Pomodoro Technique** for focus mode(25 mins) and diffuse modes(break 5 mins)

5.  **Hiker Analogy** - Take breaks while climbing up
 
 
## Question 4

## What are some of the steps that you can take to improve your learning process?

 My implementation is as follows:
1.  **Switching between Focus and Diffused Modes**: When tackling a new subject, switch between focused mode (where you're actively engaged in learning) and diffused mode (where you're allowing your mind to wander and process information).
2.  **Imagination and Creativity**: Use your imagination to come up with analogies and metaphors that relate to the concept you're trying to learn. This can help you understand the information better.
3.  **Tackling Procrastination**: Use the Pomodoro technique (breaking up study time into focused 25-minute intervals with 5-minute breaks in between) to help overcome procrastination and stay on track.
4.  **Pomodoro Technique**: for focus mode(25 mins) and diffuse modes(break 5 mins)
5.  **Hiker Analogy**: Like a hiker taking breaks and resting along the trail, take breaks when studying to allow your brain to process and retain information.

 
# Learn Anything in 20 hours

  

## By Josh Kaufman

## Question 5

## Your key takeaways from the video? Paraphrase your understanding.


### Key Points

1. Breaking down of **10000 hour rule** paradigm for learning things.

2. Balancing Practice Time and Performance Time gives best results as depicted in learning curve.

3. Balancing the learning curve on How good you are VS practice time.

4. Research about **new 20 hours rule** balanced time which is enough not just to get basics of anything but actually getting good at it.

5. This 20 hours rule is followed by Josh Kaufman.

6. He ends the talk by playing Ukulele at the end and hits his 20th hour of practicing Ukulele.

## Question 6

## What are some of the steps that you can while approaching a new topic?

### Steps:
#### 1. Collect resources

* First collect the information or resources whichever are necesssary to get started.

#### 2.Destructuring Skill

* Simplify any complicated skill into set of skills by splitting it into milestones.

* This makes the skill to be learnt more easily and efficiently.

#### 3. Learn Enough to Self Correct

* Learn the skill enough to give self-correction while practicing.

#### 4.Remove Barriers

* Do not procrastinate .

* Remove all unnecessary distractions like TV, Internet, mobile, ...

#### 5. Have patience

* Practice atleast 20 efficient hours without distractions.
