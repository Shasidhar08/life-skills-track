# Listening and Active Communication
## Active Listening
### What are the steps/strategies to do Active Listening? 
Following are the steps/strategies to do **Active Listening**:
* Avoid unnecessary distractions
* Don't interrupt the speaker
* Focus on the speaker.
* Never Interrupt the speaker.
* Take notes.
* Always wait for the speaker to finish then reply.

## Reflective Listening
### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
#### Keypoints:
*  **Active listening:** Reflective Listening requires actively engaging in the conversation and giving your full attention to the speaker.
*  **Non-judgmental attitude:** Reflective Listening involves suspending judgment and refraining from criticizing or evaluating the speaker's thoughts or feelings.
*  **Paraphrasing:** Reflective Listening entails paraphrasing or restating the speaker's words in your own words.
*  **Reflecting emotions:** In addition to paraphrasing the speaker's words, Reflective Listening also involves acknowledging and reflecting on the speaker's emotions.
*  **Open-ended questions:** Reflective Listening often incorporates open-ended questions to encourage the speaker to elaborate on their thoughts and feelings.
*  **Empathy and validation:** Reflective Listening aims to cultivate empathy by trying to see the situation from the speaker's perspective.

## Reflection
### What are the obstacles in your listening process?
Obstacles in my listening process are:
* **Environmental Distractions:** These are the major major distractions that most of the people face and I too face these lot of times.
*  **Information overload:** When the amount of information being presented is overwhelming or complex, listening may become a struggle to absorb and process all the information effectively.


### What can you do to improve your listening?
   Things to do to improve my listening:
* Avoid places where external distractions are more.
* To do some research on complex topics beforehand to avoid information overload.
---
## Types of Communication
### When do you switch to Passive communication style in your day to day life?
Switching to passive communication style in the following situations:
*  **Avoiding conflict:** In certain situations, such as when dealing with aggressive or hostile individuals, adopting a passive communication style may help de-escalate the situation and prevent further conflict.
*  **Respecting others' boundaries:** If you perceive that someone does not wish to engage in a conversation or share personal information, you may choose to adopt a passive communication style out of respect for their boundaries.
*  **Cultural considerations:** In some cultures or social contexts, a more passive communication style is the norm and is considered respectful.
*  **Receiving feedback:** When receiving feedback or criticism, adopting a passive communication style can help you listen more openly and without becoming defensive.
###  When do you switch into Aggressive communication styles in your day to day life?
 A few situations where I switch to aggressive communication style are:
*  **High-stress situations:** When provoked, I may respond with aggression as a defensive mechanism.
*  **Frustration:** Sometimes when I am frustrated, I resort to aggressive communication as a way to vent my emotions.
### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
 *  **To Conflict avoidance:** In situations when I feel threatened or attacked, I may resort to passive-aggressive behavior as a way to protect myself or regain control of the situation.
### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)
 We can make our communication assertive by keeping the following points in mind:
*  **Self-awareness:** By understanding our own thoughts, feelings, and needs before engaging in a conversation.
*  **Active listening:** Practicing attentive listening, seeking to understand the perspectives and needs of others. This helps create an environment of mutual respect and facilitates effective communication.
*  **Body Language:** Using confident and open body language, maintaining appropriate eye contact, standing or sitting upright, and using gestures that convey attentiveness and engagement.
*  **Respect boundaries:** Recognizing and respecting the boundaries of others while maintaining our own.
