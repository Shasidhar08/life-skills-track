
Q1: What kinds of behaviour cause sexual harassment?

-   Sexual harassment has three forms: verbal, visual, and physical.
-   Verbal harassment involves commenting on a person's clothes and body, making sexual and gender-based jokes, and repeatedly requesting sexual favors.
-   Spreading rumors about a person's sexual life and using foul or offensive language also constitute verbal harassment.
-   Visual harassment includes displaying posters, drawing explicit pictures, using inappropriate screensavers or cartoons, and sending explicit emails.
-   Physical harassment encompasses acts of sexual assault, impeding or blocking someone's movement, and engaging in inappropriate touching such as kissing or hugging.
-   Additionally, creating a hostile work environment by using promotions, demotions, firings, or other penalties to enforce sexual demands is considered a form of sexual harassment.

Q2: What would you do in case you face or witness any incident or repeated incidents of such behaviour?

-   Take reasonable steps to prevent or minimize bullying and harassment in the workplace.
-   Complain or inform about the incident to the mentors
-   Will ask for apology.
-   Avoid engaging in workplace bullying and harassment.
-   Would not involve in it as bullying and harassment pose risks to the health and safety of others.
