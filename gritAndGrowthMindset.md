**Grit and Growth Mindset**

**Question 1 Answer: Summarize the video in a few lines using your own words**

The video tells the story of a young teacher who discovered that intelligence alone does not determine success. Having grit and a growth mindset, which involve perseverance, effort, and the belief that abilities can be developed, are crucial for achieving success in learning.

**Question 2 Answer: What are the key takeaways from the video that we can act on?**

- Grit and a growth mindset are important for learning and success.
- We can change our mindset to foster growth and improve our learning abilities.
- Learning takes time and patience; it's important not to rush the process.

**Question 3 Answer: Summarize the video in a few lines using your own words.**

The video explores Carol Dweck's research on two mindsets: the fixed mindset and the growth mindset. The fixed mindset believes abilities are innate and unchangeable, while the growth mindset believes abilities can be developed over time. Embracing a growth mindset has numerous benefits.

**Question 4 Answer: What are the key takeaways from the video that we can act on?**

- By embracing a growth mindset and putting in effort, we can improve learning abilities and experience personal growth.
- Prioritizing the learning process and learning from challenges and mistakes leads to long-term success.
- Feedback plays a crucial role in continuous improvement and fostering a growth mindset.

**Question 5 Answer: What is the Internal Locus of Control? What is the key point in the video?**

The internal locus of control refers to the belief that we have control over our lives and outcomes. The video discusses different mindsets, including the god-gifted mindset, hard-working mindset, and lazy mindset. Those with an internal locus of control, who are hard-working and enjoy challenges, are more motivated and likely to succeed.

**Question 6 Answer: Summarize the video in a few lines using your own words.**

The video explores the acceptance mindset and unacceptance mindset. Recognizing that we can improve and taking action leads to growth. A fixed mindset perceives oneself as unchangeable, resulting in a lack of motivation for making improvements.

**Question 7 Answer: What are the key takeaways from the video that we can act on?**

- Embrace the belief in continuous improvement and growth throughout life.
- Challenge limiting assumptions to unlock full potential.
- Create a personal life curriculum aligned with passions and dreams.
- Value and learn from struggles and failures, nurturing a passion for ongoing growth.

**Question 8 Answer: What are one or more points that you want to take action on from the manual?**

- Take full responsibility for the learning journey.
- Assume ownership of assigned projects, ensuring execution, delivery, and functionality.
- Approach new challenges and interactions with enthusiasm and a positive attitude.
- Complete code only when it meets the criteria of functionality, readability, understandability, and optimization for efficiency.
