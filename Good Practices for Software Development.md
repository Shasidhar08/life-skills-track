# Good Practices for Software Development

### 1)What is your one major takeaway from each one of the 6 sections. So 6 points in total.
* Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page.
* Implementation taking longer than usual.
* Explain the problem clearly, mention the solutions you tried out to fix the problem.
* Join the meetings 5-10 mins early to get some time with your team members.
* Be available when someone replies to your message. 
* Preserving attention is a skill that you can get better with time.


### 2)Which area do you think you need to improve on? What are your ideas to make progress in that area?

* Making notes while discussing with team mates.
* Have to manage my time efficiently.
* Have to explain my problem clearly and the steps I have taken in trying to solve it.
* Be eager to learn asking seniors or employers questions and expressing your desire to collaborate with additional departments.

