# Data Types and Data Structures in JS
## Data Types  
**JavaScript** is a _dynamic_ and _weak_ typed language, which means variables can hold values of different data types and primitives are as follows
* Null - It represents intentional absence of any object or can also be assumed as 0.
```js
let  variable = null;
let  variable2 = null;
//arithematic operations with null
console.log(typeof  variable); //output: object
console.log(variable); //output: null
console.log(variable + variable2); //output: 0
console.log(variable * 10); //output: 0
console.log(variable + 10); //output: 10
``` 
* Undefined - It means variable is declared but not defined or assigned any type of value to it.
 ```js
let  undefined_variable;
console.log(typeof  undefined_variable); //output: object
console.log(undefined_variable); //output: undefined
console.log(undefined_variable * 10); //output: NaN
console.log(undefined_variable + 10); //output: NaN
/*NaN is value representing a given variable as not a
number and hence no arithematic operations can be performed on it. */
```
* Boolean - true or false value
```js
let isChecked = true;
let hasPermission = false;
console.log(isChecked); //output: true
console.log(hasPermission); //output: false
```
* Number - any number
```js
let age = 25;
let pi = 3.14; 
//typeof keyword gives the type of the given.  
console.log(typeof  age + " " + age); //output: number 25
console.log(typeof  pi + " " + pi); //output: number 3.14
```
* String - represents any text type data in quotes
```js
let name = "John Doe";
let message = 'Hello, World!'; 
```

## Data Structures 
### 1. Object  
json object is a lightweight data-interchange format.
```js
let person = {
  name: "John Doe",
  age: 25,
  isEmployed: true
};
console.log(person);
//output: {name: 'John Doe', age: 25, isEmployed: true}

``` 
### 2. Array 
This stores the values in order and are referenced by index with starting index as 0.
```js
let numbers = [1, 2, 3, 4, 5];
console.log(numbers);
//output: (5) [1, 2, 3, 4, 5]
// A collection of the values 1, 2 and 3
const arr = [1, 2, 3]

// Each value is related to one another, in the sense that each is indexed in a position of the array
const indexOfTwo = arr.indexOf(2)
console.log(arr[indexOfTwo-1]) // 1
console.log(arr[indexOfTwo+1]) // 3

// We can perform many operations on the array, like pushing new values into it
arr.push(4)
console.log(arr) // [1,2,3,4]
```

### 3.Set
* Used to sort data of any length and kind.
* Order is not guaranteed.
* Duplicate elements are not stored.
* Elements cannot be accessed by index.
```js 
let uniqueNumbers = new Set([1, 2, 3, 4, 5]);
console.log(uniqueNumbers);
//output: Set(5) {size: 5, 1, 2, 3, 4, 5}

const fruits = new Set(['apple'.'mango']);
fruits.add('banana');
console.log(fruits)
//Set(3) {'apple'. 'mango', 'banana'}
``` 

### 4. Map
* Stores key-value pairs. 
* Keys are unique.
* Values are accessed by their respective Keys.
```js
let ages = new Map();
ages.set("John", 25);
ages.set("Jane", 30);
ages.set("Mike", 40);
console.log(ages);
// Map(3) {size: 3, John => 25, Jane => 30, Mike => 40}

``` 

### 5. Stack
* Follows _last in first out_ pattern
```js 
let stack = [];
stack.push(1);
stack.push(2);
stack.push(3);
let topElement = stack.pop(); // 3
``` 

### 6. Queue
* Follows _first in first out_ pattern
```js
let queue = [];
queue.push(1);
queue.push(2);
queue.push(3);
let firstElement = queue.shift(); // 1
```
**There are also some other DataStructuers In JavaScript but these are the most used and others can be refered from below links**
#### References :
* [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)
* [Map and Set - freeCodeCamp](https://www.freecodecamp.org/news/set-and-map-in-javascript/)
* [DataStructures - freeCodeCamp](https://www.freecodecamp.org/news/data-structures-in-javascript-with-examples/)
